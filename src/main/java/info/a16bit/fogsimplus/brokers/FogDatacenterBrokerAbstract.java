package info.a16bit.fogsimplus.brokers;

import org.cloudbus.cloudsim.brokers.DatacenterBrokerSimple;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.CloudSimTags;
import org.cloudbus.cloudsim.datacenters.Datacenter;

import java.util.Set;
import java.util.TreeSet;

abstract public class FogDatacenterBrokerAbstract extends DatacenterBrokerSimple {
    public FogDatacenterBrokerAbstract(CloudSim simulation) {
        super(simulation);
    }

    public FogDatacenterBrokerAbstract(CloudSim simulation, String name) {
        super(simulation, name);
    }

    abstract public void addDatacenter(Datacenter datacenter);
}
