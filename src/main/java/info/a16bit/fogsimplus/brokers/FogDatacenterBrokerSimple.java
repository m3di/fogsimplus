package info.a16bit.fogsimplus.brokers;

import org.cloudbus.cloudsim.brokers.DatacenterBrokerSimple;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.CloudSimTags;
import org.cloudbus.cloudsim.datacenters.Datacenter;

import java.util.Set;
import java.util.TreeSet;

public class FogDatacenterBrokerSimple extends FogDatacenterBrokerAbstract {
    Set<Datacenter> datacenters = new TreeSet<>();

    public FogDatacenterBrokerSimple(CloudSim simulation) {
        super(simulation);
    }

    public FogDatacenterBrokerSimple(CloudSim simulation, String name) {
        super(simulation, name);
    }

    @Override
    public void startEntity() {
        LOGGER.info("{} is starting...", getName());
        super.send(this, 0, CloudSimTags.DATACENTER_LIST_REQUEST, datacenters);
    }

    public void addDatacenter(Datacenter datacenter) {
        datacenters.add(datacenter);
    }
}
