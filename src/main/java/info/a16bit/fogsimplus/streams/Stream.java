package info.a16bit.fogsimplus.streams;

import info.a16bit.fogsimplus.hierarchy.HierarchicalRequest;
import info.a16bit.fogsimplus.core.FogSimEntity;
import info.a16bit.fogsimplus.core.FogsimPlus;
import info.a16bit.fogsimplus.core.Tags;
import javafx.util.Pair;
import org.cloudbus.cloudsim.core.CloudSimEntity;
import org.cloudbus.cloudsim.core.events.CloudSimEvent;
import org.cloudbus.cloudsim.core.events.SimEvent;

public class Stream extends CloudSimEntity {
    protected long bandwidth;

    protected long bytesSent = 0;
    protected long bytesReceived = 0;

    protected FogSimEntity entity;

    public Stream(long bandwidth) {
        super(FogsimPlus.getSimulation());
        this.bandwidth = bandwidth;
    }

    public long getBandwidth() {
        return bandwidth;
    }

    public double propagationTime(Stream dest, long fileSize) {
        return (((double) fileSize) / Math.min(bandwidth, dest.getBandwidth()));
    }

    public void propagate(FogSimEntity receiver, HierarchicalRequest request, double delay) {
        propagate(
                receiver,
                propagationTime(receiver.getNetworkStream(), request.getTuple().getFileSize()) + delay,
                request.getTuple().getFileSize(),
                Tags.HIERARCHICAL_REQUEST_ARRIVAL,
                request
        );
    }

    public void propagate(FogSimEntity receiver, double delay, long fileSize, int tag, Object data) {
        bytesSent += fileSize;
        receiver.getNetworkStream().bytesReceived += fileSize;

        FogsimPlus.addNetworkUsage(fileSize);

        send(
                receiver.getNetworkStream(),
                delay,
                Tags.STREAM_PROPAGATION,
                new CloudSimEvent(delay, entity, receiver, tag, data)
        );
    }

    public long getBytesSent() {
        return bytesSent;
    }

    public long getBytesReceived() {
        return bytesReceived;
    }

    public FogSimEntity getEntity() {
        return entity;
    }

    public Stream setEntity(FogSimEntity entity) {
        this.entity = entity;
        return this;
    }

    @Override
    protected void startEntity() {

    }

    @Override
    public void processEvent(SimEvent evt) {
        switch (evt.getTag()) {
            case Tags.STREAM_PROPAGATION: {
                CloudSimEvent ev = (CloudSimEvent) evt.getData();
                entity.processEvent(ev);
                break;
            }
        }
    }
}
