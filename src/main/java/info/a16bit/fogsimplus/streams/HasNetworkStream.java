package info.a16bit.fogsimplus.streams;

import org.cloudbus.cloudsim.core.CloudSimEntity;

public interface HasNetworkStream {
    abstract Stream getNetworkStream();
}
