package info.a16bit.fogsimplus.physical;

import info.a16bit.fogsimplus.hierarchy.HierarchicalRequest;
import info.a16bit.fogsimplus.hierarchy.ProcessingHierarchy;
import info.a16bit.fogsimplus.application.TupleFactory;
import info.a16bit.fogsimplus.core.FogSimEntity;
import info.a16bit.fogsimplus.core.FogsimPlus;
import info.a16bit.fogsimplus.core.Tags;
import org.cloudbus.cloudsim.core.events.SimEvent;
import org.cloudbus.cloudsim.distributions.ContinuousDistribution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

// @todo check enable/disable state
public class Sensor extends FogSimEntity {
    Logger LOGGER = LoggerFactory.getLogger(Entity.class.getSimpleName());

    protected String name;

    List<HierarchicalRequest> requestSentList = new ArrayList<>();
    protected ContinuousDistribution transmitDistribution;
    protected ProcessingHierarchy hierarchy;
    protected TupleFactory tupleFactory;


    protected boolean enabled = false;

    public Sensor(String name) {
        super(FogsimPlus.getSimulation());
        this.name = name;
    }

    public int getSentTupleCount() {
        return requestSentList.size();
    }

    @Override
    protected void startEntity() {
        if (enabled) {
            send(this, 0, Tags.SENSOR_TRANSMIT);
        }
    }

    @Override
    public void processEvent(SimEvent evt) {
        switch (evt.getTag()) {
            case Tags.SENSOR_TRANSMIT:
                if (enabled) {
                    if (FogsimPlus.getSimulation().clock() < FogsimPlus.SIMULATION_TIME) {
                        transmit();
                        send(this, transmitDistribution.sample(), Tags.SENSOR_TRANSMIT);
                    }
                }
                break;
        }
    }

    protected void transmit() {
        LOGGER.info("{}: {}: Sent request", getSimulation().clockStr(), this.getName());
        HierarchicalRequest request = new HierarchicalRequest(tupleFactory.build());
        requestSentList.add(request);
        hierarchy.submitUpwards(this, request.addStation(this));
    }

    public Sensor setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public Sensor setTransmitDistribution(ContinuousDistribution transmitDistribution) {
        this.transmitDistribution = transmitDistribution;
        return this;
    }

    public Sensor setTupleFactory(TupleFactory tupleFactory) {
        this.tupleFactory = tupleFactory;
        return this;
    }

    public TupleFactory getTupleFactory() {
        return tupleFactory;
    }

    public Sensor setHierarchy(ProcessingHierarchy hierarchy) {
        this.hierarchy = hierarchy;
        return this;
    }

    public ProcessingHierarchy getHierarchy() {
        return hierarchy;
    }

    @Override
    public ProcessingHierarchy getProcessingHierarchy() {
        return hierarchy;
    }

    @Override
    public String getName() {
        return name;
    }
}
