package info.a16bit.fogsimplus.physical;

import info.a16bit.fogsimplus.core.FogSimEntity;
import info.a16bit.fogsimplus.core.FogsimPlus;
import info.a16bit.fogsimplus.core.Tags;
import info.a16bit.fogsimplus.hierarchy.HierarchicalRequest;
import info.a16bit.fogsimplus.hierarchy.ProcessingHierarchy;
import org.cloudbus.cloudsim.core.events.SimEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Actuator extends FogSimEntity {
    Logger LOGGER = LoggerFactory.getLogger(Entity.class.getSimpleName());

    protected String name;

    List<HierarchicalRequest> requestReceivedList = new ArrayList<>();

    public Actuator(String name) {
        super(FogsimPlus.getSimulation());
        this.name = name;
    }

    @Override
    protected void startEntity() {

    }

    @Override
    public void processEvent(SimEvent evt) {
        switch (evt.getTag()) {
            case Tags.HIERARCHICAL_REQUEST_ARRIVAL: {
                handleRequestArrival((HierarchicalRequest) evt.getData());
                break;
            }
        }
    }

    protected void handleRequestArrival(HierarchicalRequest request) {
        LOGGER.info("{}: {}: Received request", getSimulation().clockStr(), this.getName());
        request.processed(this);
        request.addStation(this);
        requestReceivedList.add(request);
    }

    @Override
    public ProcessingHierarchy getProcessingHierarchy() {
        return null;
    }

    @Override
    public String getName() {
        return name;
    }

    public int getReceivedTupleCount() {
        return requestReceivedList.size();
    }

    public double getMeanDelay() {
        if (requestReceivedList.size() < 1) {
            return 0;
        }

        return requestReceivedList.stream()
                .map(HierarchicalRequest::totalDelay)
                .reduce(0.0, (acc, elm) -> acc + elm) /
                requestReceivedList.size();
    }

    public double getMinDelay() {
        if (requestReceivedList.size() < 1) {
            return 0;
        }

        return requestReceivedList.stream()
                .map(HierarchicalRequest::totalDelay)
                .min(Comparator.comparingDouble(Double::valueOf))
                .get();
    }

    public double getMaxDelay() {
        if (requestReceivedList.size() < 1) {
            return 0;
        }

        return requestReceivedList.stream()
                .map(HierarchicalRequest::totalDelay)
                .max(Comparator.comparingDouble(Double::valueOf))
                .get();
    }
}
