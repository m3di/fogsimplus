package info.a16bit.fogsimplus.physical;

import info.a16bit.fogsimplus.application.ModuleFactory;
import info.a16bit.fogsimplus.application.Tuple;
import info.a16bit.fogsimplus.application.TupleFactory;
import info.a16bit.fogsimplus.brokers.FogDatacenterBrokerAbstract;
import info.a16bit.fogsimplus.brokers.FogDatacenterBrokerSimple;
import info.a16bit.fogsimplus.core.FogSimEntity;
import info.a16bit.fogsimplus.core.FogsimPlus;
import info.a16bit.fogsimplus.core.Tags;
import info.a16bit.fogsimplus.hierarchy.HierarchicalRequest;
import info.a16bit.fogsimplus.hierarchy.ProcessingHierarchy;
import javafx.util.Pair;
import org.cloudbus.cloudsim.allocationpolicies.VmAllocationPolicy;
import org.cloudbus.cloudsim.allocationpolicies.VmAllocationPolicySimple;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.core.events.SimEvent;
import org.cloudbus.cloudsim.datacenters.Datacenter;
import org.cloudbus.cloudsim.datacenters.DatacenterSimple;
import org.cloudbus.cloudsim.hosts.Host;
import org.cloudbus.cloudsim.hosts.HostSimple;
import org.cloudbus.cloudsim.power.models.PowerModel;
import org.cloudbus.cloudsim.power.models.PowerModelSpecPowerIbmX3250XeonX3480;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.ResourceProvisionerSimple;
import org.cloudbus.cloudsim.resources.Pe;
import org.cloudbus.cloudsim.resources.PeSimple;
import org.cloudbus.cloudsim.schedulers.vm.VmSchedulerTimeShared;
import org.cloudbus.cloudsim.vms.Vm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class Entity extends FogSimEntity {
    Logger LOGGER = LoggerFactory.getLogger(Entity.class.getSimpleName());

    protected String name;
    protected FogDatacenterBrokerAbstract broker;
    protected Datacenter datacenter;

    protected Map<ModuleFactory, Vm> moduleMap = new HashMap<>();
    protected Map<TupleFactory, Pair<Actuator, Double>> actuatorMap = new HashMap<>();

    protected ProcessingHierarchy processingHierarchy;

    public Entity(String name, int pes, double mips, long ram, long bw, long storage) {
        super(FogsimPlus.getSimulation());

        this.name = name;

        broker = createBroker(name);

        datacenter = createDatacenter(pes, mips, ram, bw, storage);
        datacenter.setName(name);

        this.broker.addDatacenter(this.datacenter);
    }

    protected FogDatacenterBrokerAbstract createBroker(String name) {
        return new FogDatacenterBrokerSimple(FogsimPlus.getSimulation(), name + "_broker");
    }

    protected Datacenter createDatacenter(int pes, double mips, long ram, long bw, long storage) {
        return new DatacenterSimple(FogsimPlus.getSimulation(), createHostList(pes, mips, ram, bw, storage), createVmAllocationPolicy())
                .setSchedulingInterval(10);
    }

    protected VmAllocationPolicy createVmAllocationPolicy() {
        return new VmAllocationPolicySimple();
    }

    protected List<? extends Host> createHostList(int pes, double mips, long ram, long bw, long storage) {
        return new ArrayList<Host>() {{
            add((new HostSimple(ram, bw, storage, createPeList(pes, mips)))
                    .setPowerModel(createPowerModel())
                    .setRamProvisioner(new ResourceProvisionerSimple())
                    .setBwProvisioner(new ResourceProvisionerSimple())
                    .setVmScheduler(new VmSchedulerTimeShared()));
        }};
    }

    protected List<Pe> createPeList(int pes, double mips) {
        return new ArrayList<Pe>() {{
            for (int i = 0; i < pes; i++) {
                add(new PeSimple(mips, new PeProvisionerSimple()));
            }
        }};
    }

    protected PowerModel createPowerModel() {
        return new PowerModelSpecPowerIbmX3250XeonX3480();
    }

    public void submitVmList(List<Vm> vmList) {
        this.broker.submitVmList(vmList);
    }

    public void submitCloudletList(List<Cloudlet> cloudletList) {
        this.broker.submitCloudletList(cloudletList);
    }

    public Iterable<? extends Host> getHosts() {
        return datacenter.getHostList();
    }

    public String getName() {
        return name;
    }

    @Override
    protected void startEntity() {

    }

    public void addModule(ModuleFactory module) {
        if (!moduleMap.containsKey(module)) {
            moduleMap.put(module, null);
        }
    }

    protected boolean handleRequest(HierarchicalRequest request) {
        Tuple tuple = request.getTuple();

        for (ModuleFactory sourceModule : moduleMap.keySet()) {
            final TupleFactory[] responses = sourceModule.responseTo(tuple);

            if (responses != null && responses.length > 0) {
                tuple.addOnFinishListener(info -> {
                    request.processed(this);
                    for (TupleFactory response : responses) {
                        if (response != null) {
                            processableUnitArrived(request.next(response.build()));
                        }
                    }
                });
                runOnModule(sourceModule, tuple);
                return true;
            }
        }

        return false;
    }

    protected void processableUnitArrived(HierarchicalRequest request) {
        boolean handled =
                handleRequest(request) ||
                        sendRequestToActuators(request) ||
                        sendRequestForward(request) ||
                        sendRequestBackward(request);

        if (!handled) {
            LOGGER.warn("{}: {}: nobody can't handle this request up and downwards", getSimulation().clockStr(), this.getName());
        }
    }

    public boolean canConsume(Tuple tuple) {
        for (ModuleFactory sourceModule : moduleMap.keySet()) {
            final TupleFactory[] responses = sourceModule.responseTo(tuple);
            if (responses != null && responses.length > 0) {
                return true;
            }
        }

        return actuatorMap.containsKey(tuple.getGeneratorFactory());
    }

    protected void runOnModule(ModuleFactory factory, Tuple tuple) {
        Vm vm = null;

        if (moduleMap.containsKey(factory)) {
            vm = moduleMap.get(factory);
        }

        if (vm == null) {
            vm = factory.buildVm();
            moduleMap.put(factory, vm);
            submitVm(factory, vm);
        }

        tuple.setVm(vm);
        broker.submitCloudlet(tuple);
    }

    protected void submitVm(final ModuleFactory factory, final Vm vm) {
        vm.addOnHostDeallocationListener(info -> {
            moduleMap.put(factory, null);
        });
        vm.addOnCreationFailureListener(info -> broker.getVmWaitingList().remove(vm));

        broker.submitVm(vm);
    }

    @Override
    public void processEvent(SimEvent evt) {
        switch (evt.getTag()) {
            case Tags.HIERARCHICAL_REQUEST_ARRIVAL: {
                processableUnitArrived((HierarchicalRequest) evt.getData());
                break;
            }
        }
    }

    protected boolean sendRequestToActuators(HierarchicalRequest request) {
        TupleFactory factory = request.getTuple().getGeneratorFactory();

        if (actuatorMap.containsKey(factory)) {
            Pair<Actuator, Double> pair = actuatorMap.get(factory);
            getNetworkStream().propagate(pair.getKey(), request, pair.getValue());
            return true;
        }

        return false;
    }

    protected boolean sendRequestForward(HierarchicalRequest request) {
        if (processingHierarchy != null && processingHierarchy.canConsume(request)) {
            processingHierarchy.submitUpwards(this, request.addStation(this));
            return true;
        }

        return false;
    }

    protected boolean sendRequestBackward(HierarchicalRequest request) {
        Iterator<FogSimEntity> iterator = request.iteratPathBackward();

        while (iterator.hasNext()) {
            FogSimEntity next = iterator.next();

            try {
                Entity n = (Entity) next;
                if (n.canConsume(request.getTuple())) {
                    FogSimEntity ret = request.removeStation();
                    ret.getProcessingHierarchy().submitDownwards(this, ret, request);
                    return true;
                }
            } catch (ClassCastException ignored) {
            }
        }

        return false;
    }

    public FogDatacenterBrokerAbstract getBroker() {
        return broker;
    }

    public Entity setProcessingHierarchy(ProcessingHierarchy hierarchy) {
        this.processingHierarchy = hierarchy;
        return this;
    }

    public ProcessingHierarchy getProcessingHierarchy() {
        return processingHierarchy;
    }

    public Entity addActuator(Actuator actuator, TupleFactory tupleFactory, double delay) {
        actuatorMap.put(tupleFactory, new Pair<>(actuator, delay));
        return this;
    }
}
