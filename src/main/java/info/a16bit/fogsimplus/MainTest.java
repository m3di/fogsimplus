package info.a16bit.fogsimplus;

import info.a16bit.fogsimplus.application.Module;
import info.a16bit.fogsimplus.application.ModuleFactory;
import info.a16bit.fogsimplus.application.TupleFactory;
import info.a16bit.fogsimplus.core.FogSimEntity;
import info.a16bit.fogsimplus.core.FogsimPlus;
import info.a16bit.fogsimplus.hierarchy.ProcessingHierarchy;
import info.a16bit.fogsimplus.hierarchy.ProcessingHierarchyUpstream;
import info.a16bit.fogsimplus.hierarchy.ProcessingHierarchyUpstreamWithHelperNodes;
import info.a16bit.fogsimplus.physical.Actuator;
import info.a16bit.fogsimplus.physical.Entity;
import info.a16bit.fogsimplus.physical.Sensor;
import info.a16bit.fogsimplus.streams.Stream;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.distributions.NormalDistr;
import org.cloudbus.cloudsim.hosts.Host;
import org.cloudbus.cloudsim.power.models.PowerAware;

import java.util.DoubleSummaryStatistics;
import java.util.Map;

public class MainTest {
    public static void main(String[] args) {
        CloudSim simulation = new CloudSim();
        FogsimPlus.init(simulation);

        // first make your devices (devices / fogs / clouds)

        Entity s7 = (Entity) new Entity("galaxy_s7", 2, 10000, 2048, 100000, 1000000)
                .setNetworkStream(new Stream(10000));

        Entity s8 = (Entity) new Entity("galaxy_s8", 2, 10000, 2048, 100000, 1000000)
                .setNetworkStream(new Stream(10000));

        Entity fog = (Entity) new Entity("fogDevice", 1, 10000, 4096, 100000, 1000000)
                .setNetworkStream(new Stream(10000));

        Entity cloud = (Entity) new Entity("cloudDevice", 1, 10000, 4096, 100000, 1000000)
                .setNetworkStream(new Stream(10000));

        // second make a hierarchy between them

        ProcessingHierarchy hierarchyHierarchy = new ProcessingHierarchyUpstreamWithHelperNodes(fog, 20, new Entity[]{s7, s8}, 5);
        s7.setProcessingHierarchy(hierarchyHierarchy);
        s8.setProcessingHierarchy(hierarchyHierarchy);

//        ProcessingHierarchy hierarchyHierarchy = new ProcessingHierarchyUpstream(fog, 20);
//        s7.setProcessingHierarchy(hierarchyHierarchy);
//        s8.setProcessingHierarchy(hierarchyHierarchy);

        ProcessingHierarchy cloudHierarchy = new ProcessingHierarchyUpstream(cloud, 20);
        fog.setProcessingHierarchy(cloudHierarchy);

        // then make your modules

        ModuleFactory classifier = new ModuleFactory().setMips(1000).setRam(512).setPes(1).setBw(1000).setSize(10000);
        ModuleFactory avatarGenerator = new ModuleFactory().setMips(1000).setRam(512).setPes(1).setBw(1000).setSize(10000);
        ModuleFactory dataCompressor = new ModuleFactory().setMips(1000).setRam(512).setPes(1).setBw(1000).setSize(10000);

        // then make tuples

        TupleFactory sensorData = new TupleFactory().setLength(5000).setPes(1).setFileSize(1024).setOutputSize(1024);
        TupleFactory classData = new TupleFactory().setLength(5000).setPes(1).setFileSize(1024).setOutputSize(1024);
        TupleFactory avatarData = new TupleFactory().setLength(5000).setPes(1).setFileSize(1024).setOutputSize(1024);
        TupleFactory storageData = new TupleFactory().setLength(5000).setPes(1).setFileSize(1024).setOutputSize(1024);

        // then till modules to consume a tuple and generate anothers

        classifier.addProcess(sensorData, classData);
        avatarGenerator.addProcess(classData, new TupleFactory[]{avatarData, storageData});
        dataCompressor.addProcess(storageData);

        // then place modules on devices

        s7.addModule(classifier);
        s7.addModule(avatarGenerator);
        s8.addModule(classifier);
//        s8.addModule(avatarGenerator);
//        fog.addModule(avatarGenerator);
        cloud.addModule(dataCompressor);

        // then make sensors and attach them to devices

        Sensor sensor1 = ((Sensor) new Sensor("camera-1")
                .setNetworkStream(new Stream(10000)))
                .setTransmitDistribution(new NormalDistr(20, 2))
                .setTupleFactory(sensorData)
                .setHierarchy(new ProcessingHierarchyUpstream(s7, 10))
                .setEnabled(true);

        Sensor sensor2 = ((Sensor) new Sensor("camera-2")
                .setNetworkStream(new Stream(10000)))
                .setTransmitDistribution(new NormalDistr(10, 2))
                .setTupleFactory(sensorData)
                .setHierarchy(new ProcessingHierarchyUpstream(s8, 20))
                .setEnabled(true);

        // then make actuators and attach them to devices

        Actuator miniDisplay1 = (Actuator) new Actuator("mini-display-1")
                .setNetworkStream(new Stream(10000));

        Actuator miniDisplay2 = (Actuator) new Actuator("mini-display-1")
                .setNetworkStream(new Stream(10000));

        s7.addActuator(miniDisplay1, avatarData, 30);
        s8.addActuator(miniDisplay2, avatarData, 30);

        // it's time to start simulation

        simulation.start();

        // now print the results

        printSensorStatistics(sensor1);
        printSensorStatistics(sensor2);
        printHostCpuUtilizationAndPowerConsumption(simulation, s7);
        printHostCpuUtilizationAndPowerConsumption(simulation, s8);
        printHostCpuUtilizationAndPowerConsumption(simulation, fog);
        printHostCpuUtilizationAndPowerConsumption(simulation, cloud);
        printActuatorStatistics(miniDisplay1);
        printActuatorStatistics(miniDisplay2);

        System.out.println("");
        System.out.printf("total network usage: %d Bytes%n", FogsimPlus.getNetworkUsage());
    }

    private static void printSensorStatistics(Sensor sensor) {
        System.out.println();
        System.out.printf("sensor statistics on [%s]%n", sensor.getName());
        System.out.printf("total-sent: %d%n",
                sensor.getSentTupleCount()
        );
        System.out.printf("network: sent %d Bytes / received %d Bytes%n",
                sensor.getNetworkStream().getBytesSent(),
                sensor.getNetworkStream().getBytesReceived()
        );
    }

    private static void printActuatorStatistics(Actuator actuator) {
        System.out.println();
        System.out.printf("actuator statistics on [%s]%n", actuator.getName());
        System.out.printf("total-received: %d delay [mean: %.2f min: %.2f max: %.2f]%n",
                actuator.getReceivedTupleCount(),
                actuator.getMeanDelay(),
                actuator.getMinDelay(),
                actuator.getMaxDelay()
        );
        System.out.printf("network: sent %d Bytes / received %d Bytes%n",
                actuator.getNetworkStream().getBytesSent(),
                actuator.getNetworkStream().getBytesReceived()
        );
    }

    private static void printHostCpuUtilizationAndPowerConsumption(CloudSim simulation, Entity entity) {
        System.out.println();
        System.out.printf("cloudlet statistics on %s", entity.getName());
        System.out.printf(" [submitted/finished/waiting]: %d/%d/%d%n",
                entity.getBroker().getCloudletSubmittedList().size(),
                entity.getBroker().getCloudletFinishedList().size(),
                entity.getBroker().getCloudletWaitingList().size()
        );
        System.out.printf("network: sent %d Bytes received %d Bytes%n",
                entity.getNetworkStream().getBytesSent(),
                entity.getNetworkStream().getBytesReceived()
        );
        for (Host host : entity.getHosts()) {
            final Map<Double, DoubleSummaryStatistics> utilizationPercentHistory = host.getUtilizationHistory();
            double totalWattsSec = 0;
            double utilizationHistoryTimeInterval;
            double prevTime = 0;
            for (Map.Entry<Double, DoubleSummaryStatistics> entry : utilizationPercentHistory.entrySet()) {
                utilizationHistoryTimeInterval = entry.getKey() - prevTime;
                final double utilizationPercent = entry.getValue().getSum();
                final double watts = host.getPowerModel().getPower(utilizationPercent);
                final double wattsSec = watts * utilizationHistoryTimeInterval;
                totalWattsSec += wattsSec;
                prevTime = entry.getKey();
            }

            final double energy = totalWattsSec * simulation.clock();

            System.out.printf(
                    "Total Host (%d) Power Consumption : %.0f Watt-Sec (%.5f KWatt-Hour) Energy: %.2f j %n",
                    host.getId(), totalWattsSec, PowerAware.wattsSecToKWattsHour(totalWattsSec), energy);
        }
    }
}
