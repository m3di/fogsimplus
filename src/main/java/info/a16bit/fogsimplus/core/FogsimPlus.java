package info.a16bit.fogsimplus.core;

import org.cloudbus.cloudsim.core.CloudSim;

public class FogsimPlus {
    protected static CloudSim simulation;
    protected static long networkUsage = 0;
    public static final int SIMULATION_TIME = 50000;

    public static CloudSim getSimulation() {
        return simulation;
    }

    public static void init(CloudSim simulation) {
        FogsimPlus.simulation = simulation;
    }

    public static long getNetworkUsage() {
        return networkUsage;
    }

    public static void addNetworkUsage(long networkUsage) {
        FogsimPlus.networkUsage += networkUsage;
    }
}
