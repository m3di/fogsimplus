package info.a16bit.fogsimplus.core;

public class Tags {
    protected static final int base = 50;

    public static final int TUPLE_ARRIVAL = base + 1;
    public static final int HIERARCHICAL_REQUEST_ARRIVAL = base + 2;
    public static final int SENSOR_TRANSMIT = base + 3;
    public static final int STREAM_PROPAGATION = base + 4;
}
