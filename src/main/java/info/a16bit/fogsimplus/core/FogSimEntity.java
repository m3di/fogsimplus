package info.a16bit.fogsimplus.core;

import info.a16bit.fogsimplus.hierarchy.HasProcessingHierarchy;
import info.a16bit.fogsimplus.hierarchy.ProcessingHierarchy;
import info.a16bit.fogsimplus.streams.HasNetworkStream;
import info.a16bit.fogsimplus.streams.Stream;
import org.cloudbus.cloudsim.core.CloudSimEntity;
import org.cloudbus.cloudsim.core.Simulation;

import java.util.HashMap;
import java.util.Map;

abstract public class FogSimEntity extends CloudSimEntity implements HasNetworkStream, HasProcessingHierarchy {
    protected Stream networkStream;

    public FogSimEntity(Simulation simulation) {
        super(simulation);
    }

    @Override
    public Stream getNetworkStream() {
        return networkStream;
    }

    public FogSimEntity setNetworkStream(Stream networkStream) {
        this.networkStream = networkStream.setEntity(this);
        return this;
    }
}
