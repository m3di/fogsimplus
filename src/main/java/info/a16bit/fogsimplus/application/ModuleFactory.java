package info.a16bit.fogsimplus.application;

import org.cloudbus.cloudsim.schedulers.cloudlet.CloudletScheduler;
import org.cloudbus.cloudsim.schedulers.cloudlet.CloudletSchedulerTimeShared;
import org.cloudbus.cloudsim.vms.Vm;
import org.cloudbus.cloudsim.vms.VmSimple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModuleFactory {
    protected long pes;
    protected double mips;
    protected long ram;
    protected long bw;
    protected long size;

    protected Map<TupleFactory, List<TupleFactory>> tupleMap = new HashMap<>();

    public ModuleFactory setPes(long pes) {
        this.pes = pes;
        return this;
    }

    public ModuleFactory setMips(double mips) {
        this.mips = mips;
        return this;
    }

    public ModuleFactory setRam(long ram) {
        this.ram = ram;
        return this;
    }

    public ModuleFactory setBw(long bw) {
        this.bw = bw;
        return this;
    }

    public ModuleFactory setSize(long size) {
        this.size = size;
        return this;
    }

    public Vm buildVm() {
        Vm vm = new VmSimple(mips, pes).setRam(ram).setBw(bw).setSize(size).setCloudletScheduler(createCloudletScheduler());
        vm.getUtilizationHistory().enable();
        return vm;
    }

    private CloudletScheduler createCloudletScheduler() {
        return new CloudletSchedulerTimeShared();
    }

    public void addProcess(TupleFactory receive) {
        addProcess(receive, (TupleFactory) null);
    }

    public void addProcess(TupleFactory receive, TupleFactory[] generate) {
        for (TupleFactory factory : generate) {
            addProcess(receive, factory);
        }
    }

    public void addProcess(TupleFactory receive, TupleFactory generate) {
        if (!tupleMap.containsKey(receive)) {
            tupleMap.put(receive, new ArrayList<>());
        }

        if (!tupleMap.get(receive).contains(generate)) {
            tupleMap.get(receive).add(generate);
        }
    }

    public TupleFactory[] responseTo(Tuple tuple) {
        for (Map.Entry<TupleFactory, List<TupleFactory>> entry : tupleMap.entrySet()) {
            if (entry.getKey().isGenerated(tuple)) {
                return entry.getValue().toArray(new TupleFactory[0]);
            }
        }

        return null;
    }
}
