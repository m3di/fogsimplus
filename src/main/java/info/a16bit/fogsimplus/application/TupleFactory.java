package info.a16bit.fogsimplus.application;

import org.cloudbus.cloudsim.utilizationmodels.UtilizationModel;
import org.cloudbus.cloudsim.utilizationmodels.UtilizationModelDynamic;
import org.cloudbus.cloudsim.utilizationmodels.UtilizationModelFull;

public class TupleFactory {
    protected long length;
    protected long pes;
    protected long fileSize;
    protected long outputSize;

    UtilizationModel umCpu;
    UtilizationModel umRAM;
    UtilizationModel umBW;

    public TupleFactory() {
        this.umCpu = createUtilizationModelCpu();
        this.umRAM = createUtilizationModelRam();
        this.umBW = createUtilizationModelBw();
    }

    public TupleFactory setLength(long length) {
        this.length = length;
        return this;
    }

    public TupleFactory setPes(long pes) {
        this.pes = pes;
        return this;
    }

    public TupleFactory setFileSize(long fileSize) {
        this.fileSize = fileSize;
        return this;
    }

    public TupleFactory setOutputSize(long outputSize) {
        this.outputSize = outputSize;
        return this;
    }

    public TupleFactory setUmCpu(UtilizationModel umCpu) {
        this.umCpu = umCpu;
        return this;
    }

    public TupleFactory setUmRAM(UtilizationModel umRAM) {
        this.umRAM = umRAM;
        return this;
    }

    public TupleFactory setUmBW(UtilizationModel umBW) {
        this.umBW = umBW;
        return this;
    }

    public Tuple build() {
        return (Tuple) new Tuple(length, pes)
                .setGeneratorFactory(this)
                .setFileSize(fileSize)
                .setOutputSize(outputSize)
                .setUtilizationModelCpu(umCpu)
                .setUtilizationModelRam(umRAM)
                .setUtilizationModelBw(umBW);
    }

    protected UtilizationModel createUtilizationModelCpu() {
        return new UtilizationModelFull();
    }

    protected UtilizationModel createUtilizationModelRam() {
        return new UtilizationModelDynamic(0.2);
    }

    protected UtilizationModel createUtilizationModelBw() {
        return new UtilizationModelDynamic(0.2);
    }

    public boolean isGenerated(Tuple tuple) {
        return tuple.getGeneratorFactory() == this;
    }
}
