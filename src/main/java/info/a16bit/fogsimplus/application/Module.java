package info.a16bit.fogsimplus.application;

import org.cloudbus.cloudsim.schedulers.cloudlet.CloudletScheduler;
import org.cloudbus.cloudsim.vms.VmSimple;

public class Module extends VmSimple {
    public Module(double mipsCapacity, long numberOfPes) {
        super(mipsCapacity, numberOfPes);
    }

    public Module(double mipsCapacity, long numberOfPes, CloudletScheduler cloudletScheduler) {
        super(mipsCapacity, numberOfPes, cloudletScheduler);
    }

    public Module(long id, double mipsCapacity, long numberOfPes) {
        super(id, mipsCapacity, numberOfPes);
    }

    public Module(long id, long mipsCapacity, long numberOfPes) {
        super(id, mipsCapacity, numberOfPes);
    }
}
