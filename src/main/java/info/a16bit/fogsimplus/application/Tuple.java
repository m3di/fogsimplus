package info.a16bit.fogsimplus.application;

import org.cloudbus.cloudsim.cloudlets.CloudletSimple;
import org.cloudbus.cloudsim.utilizationmodels.UtilizationModel;

public class Tuple extends CloudletSimple {
    protected TupleFactory generatorFactory = null;

    public Tuple(long length, int pesNumber, UtilizationModel utilizationModel) {
        super(length, pesNumber, utilizationModel);
    }

    public Tuple(long length, int pesNumber) {
        super(length, pesNumber);
    }

    public Tuple(long length, long pesNumber) {
        super(length, pesNumber);
    }

    public Tuple(long id, long length, long pesNumber) {
        super(id, length, pesNumber);
    }

    public TupleFactory getGeneratorFactory() {
        return generatorFactory;
    }

    public Tuple setGeneratorFactory(TupleFactory generatorFactory) {
        this.generatorFactory = generatorFactory;
        return this;
    }
}
