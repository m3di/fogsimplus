package info.a16bit.fogsimplus.hierarchy;

import info.a16bit.fogsimplus.core.FogSimEntity;
import info.a16bit.fogsimplus.physical.Entity;
import info.a16bit.fogsimplus.streams.Stream;

import java.util.Iterator;

public interface ProcessingHierarchy {
    public boolean canConsume(HierarchicalRequest request);

    public void submitUpwards(FogSimEntity sender, HierarchicalRequest request);

    public void submitDownwards(Entity sender, FogSimEntity receiver, HierarchicalRequest request);
}
