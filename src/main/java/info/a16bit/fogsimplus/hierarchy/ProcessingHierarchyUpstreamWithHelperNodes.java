package info.a16bit.fogsimplus.hierarchy;

import info.a16bit.fogsimplus.core.FogSimEntity;
import info.a16bit.fogsimplus.physical.Entity;

import java.util.Iterator;

public class ProcessingHierarchyUpstreamWithHelperNodes implements ProcessingHierarchy {
    protected Entity root;
    protected double rootDelay;

    protected Entity[] helpers;
    protected double helpersDelay;

    public ProcessingHierarchyUpstreamWithHelperNodes(Entity upstream, double upstreamDelay, Entity[] helpers, double helpersDelay) {
        this.root = upstream;
        this.rootDelay = upstreamDelay;
        this.helpers = helpers;
        this.helpersDelay = helpersDelay;
    }

    @Override
    public boolean canConsume(HierarchicalRequest request) {
        for (Entity helper : helpers) {
            if (helper.canConsume(request.getTuple())) {
                return true;
            }
        }

        return root.canConsume(request.getTuple()) || (root.getProcessingHierarchy() != null && root.getProcessingHierarchy().canConsume(request));
    }

    @Override
    public void submitUpwards(FogSimEntity sender, HierarchicalRequest request) {
        for (Entity helper : helpers) {
            if (helper.canConsume(request.getTuple())) {
                sender.getNetworkStream().propagate(helper, request, helpersDelay);
                return;
            }
        }

        sender.getNetworkStream().propagate(root, request, rootDelay);
    }

    @Override
    public void submitDownwards(Entity sender, FogSimEntity receiver, HierarchicalRequest request) {
        if (sender == root) {
            sender.getNetworkStream().propagate(receiver, request, rootDelay);
        } else {
            sender.getNetworkStream().propagate(receiver, request, helpersDelay);
        }
    }
}
