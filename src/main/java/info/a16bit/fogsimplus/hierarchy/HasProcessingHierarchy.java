package info.a16bit.fogsimplus.hierarchy;

public interface HasProcessingHierarchy {
    public ProcessingHierarchy getProcessingHierarchy();
}
