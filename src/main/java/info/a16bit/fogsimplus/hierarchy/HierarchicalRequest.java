package info.a16bit.fogsimplus.hierarchy;

import info.a16bit.fogsimplus.application.Tuple;
import info.a16bit.fogsimplus.core.FogSimEntity;
import info.a16bit.fogsimplus.core.FogsimPlus;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HierarchicalRequest {
    protected List<FogSimEntity> path = new ArrayList<>();
    protected HierarchicalRequest previous;
    protected FogSimEntity processedBy;
    protected double generatedAt;
    protected double processedAt;
    protected Tuple tuple;

    public HierarchicalRequest(Tuple tuple) {
        this.tuple = tuple;
        this.generatedAt = FogsimPlus.getSimulation().clock();
    }

    public Tuple getTuple() {
        return tuple;
    }

    public HierarchicalRequest getPrevious() {
        return previous;
    }

    public double getGeneratedAt() {
        return generatedAt;
    }

    public Double getProcessedAt() {
        return processedAt;
    }

    public HierarchicalRequest setPrevious(HierarchicalRequest previous) {
        this.previous = previous;
        return this;
    }

    public HierarchicalRequest setPath(List<FogSimEntity> path) {
        this.path = new ArrayList<>();
        this.path.addAll(path);
        return this;
    }

    public HierarchicalRequest next(Tuple next) {
        return new HierarchicalRequest(next).setPrevious(this).setPath(path);
    }

    public void processed(FogSimEntity entity) {
        this.processedBy = entity;
        this.processedAt = FogsimPlus.getSimulation().clock();
    }

    public HierarchicalRequest addStation(FogSimEntity entity) {
        path.add(entity);
        return this;
    }

    public FogSimEntity removeStation() {
        FogSimEntity ret = path.get(path.size() - 1);
        path.remove(path.size() - 1);
        return ret;
    }

    public Iterator<FogSimEntity> iteratPathBackward() {
        return new Iterator<FogSimEntity>() {
            int current = path.size() - 1;

            @Override
            public boolean hasNext() {
                return current >= 0;
            }

            @Override
            public FogSimEntity next() {
                return path.get(current--);
            }
        };
    }

    public double totalDelay() {
        HierarchicalRequest current = this;

        while (current.getPrevious() != null) {
            current = current.getPrevious();
        }

        return processedAt - current.getGeneratedAt();
    }
}
