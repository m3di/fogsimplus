package info.a16bit.fogsimplus.hierarchy;

import info.a16bit.fogsimplus.core.FogSimEntity;

public interface Closure {
    public boolean run(Closure next, FogSimEntity entity);
}
