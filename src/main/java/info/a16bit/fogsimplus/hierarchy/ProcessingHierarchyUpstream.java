package info.a16bit.fogsimplus.hierarchy;

import info.a16bit.fogsimplus.core.FogSimEntity;
import info.a16bit.fogsimplus.physical.Entity;
import info.a16bit.fogsimplus.streams.Stream;

import java.util.Iterator;

public class ProcessingHierarchyUpstream implements ProcessingHierarchy {
    protected Entity root;
    protected double delay;

    public ProcessingHierarchyUpstream(Entity root, double delay) {
        this.root = root;
        this.delay = delay;
    }

    @Override
    public boolean canConsume(HierarchicalRequest request) {
        return root.canConsume(request.getTuple()) || (root.getProcessingHierarchy() != null && root.getProcessingHierarchy().canConsume(request));
    }

    @Override
    public void submitUpwards(FogSimEntity sender, HierarchicalRequest request) {
        sender.getNetworkStream().propagate(root, request, delay);
    }

    @Override
    public void submitDownwards(Entity sender, FogSimEntity receiver, HierarchicalRequest request) {
        sender.getNetworkStream().propagate(receiver, request, delay);
    }
}
